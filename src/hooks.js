import * as cookie from 'cookie';
import { production } from './vars';
import { User, AdminLogin, database } from './routes/api/mongodb';

const verifyToken = async (token, db) => {
	if (token === db) {
		return true;
	} else {
		return false;
	}
};

let isLoggedIn;
let noAdmin = false;
let defaultLanguage;

export async function handle({ request, resolve }) {
	// Get and set user routes
	database();
	let dbCookie;
	let publicPages = ['/', '/api/data', '/login', '/auth/login'];
	const all = await User.find();
	all.forEach((item) => {
		publicPages = [...publicPages, `/user/${item.id}`];
	});
	const adminUser = await AdminLogin.find({ username: production.admin_account });
	if (adminUser.length > 0) dbCookie = adminUser[0].cookie;
	// Check cookie
	const cookies = cookie.parse(request.headers.cookie || '');
	request.locals.user = cookies;

	if (!cookies.session_id) {
		request.locals.user.isLoggedIn = false;
	}
	if (cookies) {
		request.locals.user.isLoggedIn = await verifyToken(cookies.session_id, dbCookie);
	} else {
		request.locals.user.isLoggedIn = false;
	}
	if (adminUser.length <= 0) {
		request.locals.user.isLoggedIn = false;
		request.locals.user.noAdmin = true;
	}

	isLoggedIn = request.locals.user.isLoggedIn;
	defaultLanguage = adminUser[0].language;
	const response = await resolve(request);

	if (!request.locals.user.isLoggedIn && !publicPages.includes(request.path)) {
		return {
			status: 302,
			headers: {
				location: '/'
			}
		};
	}
	return {
		...response,
		headers: {
			...response.headers
		}
	};
}

export function getSession(request) {
	/* const { isLoggedIn } = request.locals; */
	return request.locals.user
		? {
				user: {
					isLoggedIn,
					noAdmin,
					defaultLanguage
				}
		  }
		: {};
}
