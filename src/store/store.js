import { writable, get } from 'svelte/store';

export const globalMessage = writable({ message: '', color: '' });
export const globalMessageStatus = writable(false);
export const usersStore = writable([]);
export const currentView = writable('');

export async function setGlobalMessage(message, color) {
	function clearGlobalMessage() {
		globalMessage.set({ message: '', color: '' });
		globalMessageStatus.set(false);
	}
	try {
		globalMessageStatus.set(true);
		globalMessage.set({ message: message, color: color });
		setTimeout(clearGlobalMessage, 2000);
	} catch (error) {
		console.log(error);
	}
}

export const reload = async (field, id) => {
	if (field === 'all') {
		usersStore.set([]);
		const res = await fetch(`/api/data?all=all`);
		const userData = await res.json();
		userData.forEach((item) => {
			usersStore.set([...get(usersStore), item]);
		});
	} else if (field === 'me') {
		const res = await fetch(`/api/data?me=${id}`);
		const userData = await res.json();
		usersStore.set([userData]);
	}
};
