import { derived, writable } from 'svelte/store';
import es from '../es';
import en from '../en';
import { production } from '../vars';

// select language
const envLanguage = production.languageSelection;
export const locale = writable(envLanguage);

const translations = { es, en };

// all languages
export const locales = Object.keys(translations);

function translate(locale, key, vars) {
	const splitKey = key.split('.');
	let newKeys = { ...translations[locale] };
	splitKey.forEach((item) => {
		newKeys = newKeys[item];
	});
	if (!key) throw new Error('no key provided to $t()');
	if (!locale) throw new Error(`no translation for key "${key}"`);
	let text = newKeys;
	if (!text) throw new Error(`no translation found for ${locale}.${key}`);
	Object.keys(vars).map((k) => {
		const regex = new RegExp(`{{${k}}}`, 'g');
		text = text.replace(regex, vars[k]);
	});
	return text;
}

export const t = derived(
	locale,
	($locale) =>
		(key, vars = {}) =>
			translate($locale, key, vars)
);
