export default {
	add_user_box: {
		input_container: {
			username: 'Name',
			family: 'Family / Group'
		},
		button_container: {
			cancel: 'Cancel',
			save: 'Save'
		}
	},
	admin_user_box: {
		handle_save: {
			save_ok: 'has been saved!',
			save_not_ok: "Error saving {{username}}'s details! "
		},
		fields_array: {
			id: 'Id',
			username: 'Name',
			family: 'Family / Group',
			options: 'Options',
			selected: 'Selected',
			secret: 'Secret',
			secret_name: 'Secret name'
		},
		user_card_container: {
			clipboard_ok: "{{username}}'s link has been copied to clipboard!",
			clipboard_not_ok: "Error copying {{username}}'s link!: {{error}}",
			copy_link: 'Click to copy',
			share_link: 'Link to share'
		},
		button_container: {
			delete_message: 'Are you sure you want to delete {{username}}? 😨',
			cancel_message: 'edit canceled!',
			cancel: 'Cancel',
			save: 'Save',
			delete: 'Delete'
		}
	},
	image_upload: {
		validate_image: {
			invalid_file: 'This file format is invalid! please upload images in {{fileTypes}}'
		},
		upload_image: {
			upload_ok: 'Image uploaded!',
			upload_not_ok: 'Error uploading the image! :('
		}
	},
	index: {
		welcome: {
			message_1: 'What are you doing here?',
			message_2: 'Secret santa is secret...',
			message_3: '🎅 🤐'
		}
	},
	login_box: {
		handle_login: {
			login_ok: 'Logged in! ☃️',
			login_not_ok: 'Incorrect password! 🤔',
			save_ok: 'Admin account created! ☃️',
			save_not_ok: "Couldn't create the admin account! 🙃"
		},
		welcome: {
			message: 'Oh, hello there!'
		},
		button_container: {
			login: 'Login',
			save: 'Save'
		}
	},
	sidebar: {
		logout: {
			logout_ok: 'Byeeeee👋',
			logout_not_ok: "We don't want to say bye just yet"
		},
		language: {
			set_ok: 'Language set',
			set_not_ok: 'Language error set what..? :o'
		}
	},
	testing: {
		batch_upload: {
			upload_ok: '{{username}} has been uploaded',
			upload_not_ok: 'Error uploading {{username}}...',
			users_undefined: "Users array doesn't exist, create it to batch generate users: {{err}}"
		},
		get_all: 'All users have been reloaded',
		populate_all: {
			send_not_ok: "Error setting {{username}}'s secret santa"
		},
		button_container: {
			add_all: 'Add all',
			reload_all: 'Reload all',
			delete_all: 'Delete all',
			delete_images: 'Delete images',
			restart_game: 'Restart game',
			run_game: 'Run game'
		}
	},
	user_box: {
		handle_save: {
			save_ok: 'Changes have been saved!',
			save_not_ok: "There's been an error trying to save the changes!"
		},
		fields_array: {
			username: 'Name',
			preference_1: 'Gift option 1',
			preference_2: 'Gift option 2',
			preference_3: 'Gift option 3'
		},
		pick_secret_santa: {
			pick_not_ok: 'Server error! 😔',
			set_image: 'Upload a picture before picking! 📷',
			box_label: 'is gifting...'
		},
		button_container: {
			cancel: 'Cancel',
			save: 'Save'
		}
	}
};
