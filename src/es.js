export default {
	add_user_box: {
		input_container: {
			username: 'Nombre',
			family: 'Familia / Grupo'
		},
		button_container: {
			cancel: 'Cancelar',
			save: 'Guardar'
		}
	},
	admin_user_box: {
		handle_save: {
			save_ok: 'ha sido guardado!',
			save_not_ok: 'Error al guardar los detalles de {{username}}!'
		},
		fields_array: {
			id: 'Id',
			username: 'Nombre',
			family: 'Familia / Grupo',
			options: 'Opciones',
			selected: 'Seleccionado',
			secret: 'Secreto',
			secret_name: 'Nombre secreto'
		},
		user_card_container: {
			clipboard_ok: 'El link de {{username}} ha sido copiado!',
			clipboard_not_ok: 'Error al copiar el link de {{username}}!: {{error}}',
			copy_link: 'Da click para copiar',
			share_link: 'Link para compartir'
		},
		button_container: {
			delete_message: 'Seguro que quieres eliminar a {{username}}? 😨',
			cancel_message: 'edición cancelada!',
			cancel: 'Cancelar',
			save: 'Guardar',
			delete: 'Eliminar'
		}
	},
	image_upload: {
		validate_image: {
			invalid_file: 'Este formato de archivo es inválido! Los formatos válidos son {{fileTypes}}'
		},
		upload_image: {
			upload_ok: 'Se ha guardado la imagen!',
			upload_not_ok: 'Error al guardar la imagen! :('
		}
	},
	index: {
		welcome: {
			message_1: '¿Qué haces aquí?',
			message_2: 'Este juego es secreto...',
			message_3: '🎅 🤐'
		}
	},
	login_box: {
		handle_login: {
			login_ok: 'Has entrado! ☃️',
			login_not_ok: 'Contraseña incorrecta! 🤔',
			save_ok: 'Se ha creado la cuenta de administrador! ☃️',
			save_not_ok: 'No se ha podido crear la cuenta de administrador! 🙃'
		},
		welcome: {
			message: 'Oh, que tal!'
		},
		button_container: {
			login: 'Entrar',
			save: 'Save'
		}
	},
	sidebar: {
		logout: {
			logout_ok: 'Adiooooos 👋',
			logout_not_ok: 'No podemos decir adiós aun :('
		},
		language: {
			set_ok: 'Se ha cambiado el idioma',
			set_not_ok: 'Idioma error cambiar... ¿qué? :o'
		}
	},
	testing: {
		batch_upload: {
			upload_ok: '{{username}} ha sido guardado',
			upload_not_ok: 'Error al guardar los datos de {{username}}...',
			users_undefined:
				'El listado de usuarios no existe, créalo para añadirlos rápidamente: {{err}}'
		},
		get_all: 'Se ha actualiazdo la información de los usuarios',
		populate_all: {
			send_not_ok: 'Error al asignar al recipiente de {{username}}'
		},
		button_container: {
			add_all: 'Añadir todos',
			reload_all: 'Actualizar todos',
			delete_all: 'Eliminar todos',
			delete_images: 'Eliminar todas las imágenes',
			restart_game: 'Reiniciar el juego',
			run_game: 'Correr el juego'
		}
	},
	user_box: {
		handle_save: {
			save_ok: 'Se han guardado los cambios!',
			save_not_ok: 'Hubo un erro al guardar los cambios!'
		},
		fields_array: {
			username: 'Nombre',
			preference_1: 'Preferencia de regalo 1',
			preference_2: 'Preferencia de regalo 2',
			preference_3: 'Preferencia de regalo 3'
		},
		pick_secret_santa: {
			pick_not_ok: 'Hubo un error en el servidor! 😔',
			set_image: 'Sube una imagen antes de elegir! 📷',
			box_label: 'le regalará a...'
		},
		button_container: {
			cancel: 'Cancelar',
			save: 'Guardar'
		}
	}
};
