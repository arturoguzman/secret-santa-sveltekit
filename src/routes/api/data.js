import { production } from '../../vars';
import { v4 as uuid } from 'uuid';
import { User } from './mongodb';
import _, { map } from 'underscore';
import { AdminLogin } from '../api/mongodb';
const adminCrendentials = production.admin_account;

export const get = async (request) => {
	if (request.query.get('me')) {
		try {
			const query = request.query.get('me');
			const item = `${query}`;
			const user = await User.find({ id: item });
			console.log(user);
			const newBody = {
				_id: user[0]._id,
				id: user[0].id,
				username: user[0].username,
				family: user[0].family,
				options: user[0].options,
				secret: user[0].secret,
				secretImage: user[0].secretImage,
				secretName: user[0].secretName,
				secretOptions: user[0].secretOptions,
				image: user[0].image
			};

			if (newBody.secret !== undefined) {
				const secretUser = await User.find({ _id: user[0].secret });
				newBody.secretOptions = secretUser[0].options;
				newBody.secretImage = secretUser[0].image;
				newBody.secretName = secretUser[0].username;
			}
			return {
				status: 200,
				body: newBody
			};
		} catch (err) {
			console.log(err);
			return {
				status: 404,
				body: {
					error: 'id not found'
				}
			};
		}
	} else if (request.query.get('secret')) {
		try {
			const query = request.query.get('secret');
			const item = `${query}`;
			const user = await User.find({ id: item });
			const storeUser = user[0];
			const users = await User.where({ selected: false })
				.where('id')
				.ne(storeUser.id)
				.where('family')
				.ne(storeUser.family);
			const filter = _.sample(users);
			const secretId = `${filter._id}`;
			console.log(`${storeUser.username} has got ${filter.username}`);
			storeUser.secret = secretId;
			storeUser.secretName = filter.username;
			storeUser.secretOptions = filter.options;
			storeUser.secretImage = filter.image;
			filter.selected = true;
			await storeUser.save();
			await filter.save();
			return {
				status: 200,
				body: {
					message: `${storeUser.username} has got ${filter.username}`
				}
			};
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: err
				}
			};
		}
	} else if (request.query.get('all')) {
		const all = await User.find();
		return {
			body: all
		};
	}
};

export const post = async (request) => {
	if (request.query.get('new')) {
		try {
			let parsedBody = JSON.parse(request.body);
			const body = await User.create({
				id: uuid(),
				username: parsedBody.username,
				family: parsedBody.family,
				options: ['🎅', '🎄', '🎁'],
				selected: false
			});
			return {
				status: 200,
				body
			};
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: 'error :('
				}
			};
		}
	} else if (request.query.get('language')) {
		try {
			const language = request.query.get('language');
			await AdminLogin.findOneAndUpdate(
				{ username: adminCrendentials },
				{ language: language },
				{ new: true }
			);
			return {
				status: 200,
				body: 'language set'
			};
		} catch (err) {
			console.log(err);
			return {
				status: 500,
				body: {
					message: 'error :('
				}
			};
		}
	}
};

export const patch = async (request) => {
	if (request.query.get('images')) {
		try {
			await User.updateMany(
				{},
				{
					$unset: {
						image: ''
					}
				}
			);
			return {
				status: 200,
				body: {
					message: 'ok'
				}
			};
		} catch (err) {
			console.log(err);
		}
	} else if (request.query.get('reset')) {
		try {
			await User.updateMany(
				{},
				{
					$unset: {
						secret: '',
						secretName: '',
						secretOptions: ''
					},
					$set: {
						selected: false
					}
				}
			);
			return {
				status: 200,
				body: {
					message: 'ok'
				}
			};
		} catch (err) {
			console.log(err);
		}
	} else if (request.query.get('user')) {
		try {
			const filter = { id: request.query.get('user') };
			const parsedBody = JSON.parse(request.body);
			const update = {
				username: parsedBody.username,
				family: parsedBody.family,
				selected: parsedBody.selected,
				secret: parsedBody.secret,
				secretName: parsedBody.secretName
			};
			const res = await User.findOneAndUpdate(filter, update, { new: true });
			return {
				status: 200,
				body: {
					message: 'ok',
					response: res
				}
			};
		} catch (err) {
			console.log(err);
		}
	} else if (request.query.get('me')) {
		try {
			const filter = { id: request.query.get('me') };
			const parsedBody = JSON.parse(request.body);
			const update = {
				username: parsedBody.username,
				options: parsedBody.options
			};
			const res = await User.findOneAndUpdate(filter, update, { new: true });
			return {
				status: 200,
				body: {
					message: 'ok',
					response: res
				}
			};
		} catch (err) {
			console.log(err);
		}
	}
};

export const del = async (request) => {
	try {
		if (request.query.get('all')) {
			await User.deleteMany();
			return {
				status: 200,
				message: 'ok'
			};
		} else if (request.query.get('user')) {
			const userId = request.query.get('user');
			const deleteUser = await User.deleteOne({ id: userId });
			return {
				status: 200,
				body: {
					message: 'ok',
					content: deleteUser
				}
			};
		}
	} catch (err) {
		console.log(err);
		return {
			status: 500,
			body: {
				message: 'error :('
			}
		};
	}
};

// user requests own information, server selects and adds an id to secret, then fetches from secret, stores secret in secret database, and removes secret from string
