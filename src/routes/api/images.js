import sharp from 'sharp';
import { production } from '../../vars';
import { User } from './mongodb';
import { FormData, Blob } from 'formdata-node';
export const post = async (request) => {
	if (request.query.get('image')) {
		try {
			// for local deployment
			/* const name = request.query.get('image');
			await sharp(Buffer.from(request.body, 'base64'))
				.webp()
				.toFile(`./static/images/${name}.webp`);
			const user = await User.find({ _id: name });
			const storeUser = user[0];
			storeUser.image = `INSERT/IMAGE/FOLDER/${name}.webp`;
			await storeUser.save();
			return {
				status: 200,
				body: {
					message: 'ok'
				}
			}; */

			// for Directus
			const form = new FormData();
			const bufferImage = await sharp(Buffer.from(request.body, 'base64')).webp().toBuffer();
			const file = new Blob([bufferImage], { type: 'image/webp' });
			const name = request.query.get('image');
			form.append('storage', 'local');
			form.append('filename_download', name);
			form.append('filename_disk', name);
			form.append('title', name);
			form.append('file', file, `${name}.webp`);
			let headers = {};
			headers['Authorization'] = `Bearer ${production.directus_auth}`;
			const data = {
				method: 'POST',
				headers,
				body: form
			};
			const res = await fetch(production.images_upload, data);
			const imageUrl = await res.json();
			const user = await User.find({ _id: name });
			const storeUser = user[0];
			storeUser.image = `${production.images_download}/${imageUrl.data.id}`;
			await storeUser.save();

			//

			if (res.ok) {
				return {
					status: 200,
					body: {
						message: 'ok'
					}
				};
			} else {
				return {
					status: 500,
					body: {
						message: 'ohhh shite'
					}
				};
			}
		} catch (err) {
			console.log(err);
			return { status: 400, body: { message: err } };
		}
	} else {
		return {
			status: 401,
			body: {
				message: 'You are not authorised, mate'
			}
		};
	}
};
