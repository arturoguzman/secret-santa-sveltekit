import mongoose from 'mongoose';
import { production } from '../../vars';

const dbUrl = production.mongodb_url;
const dbName = production.mongodb_name;
const dbOptions = production.mongodb_options;

const url = dbUrl + dbName + dbOptions;

export const database = async () => {
	await mongoose.connect(url);
};

const userSchema = new mongoose.Schema({
	id: String,
	username: String,
	family: String,
	options: [],
	/* secret: String, */
	secret: {
		type: mongoose.SchemaTypes.ObjectId,
		ref: 'User'
	},
	image: String,
	secretName: String,
	secretOptions: [],
	selected: Boolean
});

export const User = mongoose.model('User', userSchema);

const adminSchema = new mongoose.Schema({
	username: String,
	password: String,
	cookie: String,
	language: String
});

export const AdminLogin = mongoose.model('AdminLogin', adminSchema);
