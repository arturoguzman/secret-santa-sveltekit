import { production } from '../../vars';
import bcrypt from 'bcryptjs';
import * as cookie from 'cookie';
import { AdminLogin } from '../api/mongodb';
import { v4 as uuid } from 'uuid';

const adminCrendentials = production.admin_account;

export const post = async (request) => {
	if (request.query.get('bye')) {
		return {
			status: 200,
			headers: {
				'set-cookie': 'session_id=deleted; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT'
			},
			body: {
				message: 'logged out!'
			}
		};
	} else if (request.query.get('save')) {
		try {
			const adminUser = await AdminLogin.find({ username: adminCrendentials });
			if (adminUser > 0)
				return { status: 401, body: { message: "There's an existing admin account already!" } };
			const parsedBody = JSON.parse(request.body);
			const passwd = parsedBody.input;
			let hashed = new Promise((resolve, reject) => {
				try {
					const salt = bcrypt.genSaltSync(10);
					const hash = bcrypt.hashSync(passwd, salt);
					const save = async (hash) => {
						await AdminLogin.create({
							username: adminCrendentials,
							password: hash,
							cookie: uuid(),
							language: 'en'
						});
					};
					save(hash);
					resolve('loggedIn');
				} catch (err) {
					console.log(err);
					reject(err);
				}
			});
			return {
				status: 200,
				body: {
					message: await hashed
				}
			};
		} catch (err) {
			console.log(err);
		}
	} else if (request.query.get('login')) {
		try {
			const parsedBody = JSON.parse(request.body);
			const passwd = parsedBody.input;
			const adminUser = await AdminLogin.find({ username: adminCrendentials });
			const dbpasswd = adminUser[0].password;
			let headers = {};
			let hashed = new Promise((resolve, reject) => {
				const result = bcrypt.compareSync(passwd, dbpasswd);
				if (!result) {
					console.log('bad password', result);
					resolve({ result, code: 401 });
				} else if (result) {
					console.log('good password');
					resolve({ result, code: 200, cookie: adminUser[0].cookie });
				} else {
					reject();
				}
			});

			const result = await hashed;
			/* if (result.code === 200) { */
			headers = {
				'Set-Cookie': cookie.serialize('session_id', result.cookie, {
					httpOnly: true,
					maxAge: 60 * 60 * 24,
					sameSite: 'lax',
					path: '/'
				})
			};
			/* } */
			return {
				status: result.code,
				headers,
				body: {
					message: adminUser.language
				}
			};
		} catch (err) {
			console.log(err);
		}
	}
};
