# Secret Santa - Sveltekit
Secret Santa developed with **easiness in mind**!


![](https://api.arturoguzmanperez.com/uploads/Screen_Shot_2021_12_07_at_9_35_01_AM_3c3fb1bad9.png)
![](https://api.arturoguzmanperez.com/uploads/Screen_Shot_2021_12_07_at_9_35_22_AM_ac2a3e2349.png)
![](https://api.arturoguzmanperez.com/uploads/Screen_Shot_2021_12_07_at_9_40_05_AM_1a16158695.png)
![](https://api.arturoguzmanperez.com/uploads/Screen_Shot_2021_12_07_at_9_49_57_AM_242e835a46.png)
![](https://api.arturoguzmanperez.com/uploads/Screen_Shot_2021_12_07_at_9_52_44_AM_f71a8e0e7c.png)


1. No user login
2. Image upload
3. Self-family-group restrictions
4. 3 gift preferences
5. Protected admin interface
6. Languages (english/español)
7. Testing tools
8. Desktop - mobile design

**once deployed, go to /login to create your password and access the admin server**

You will need to have a MongoDB database for user data (a free [MongoDB Atlas](https://www.mongodb.com/atlas/database) will do), and a storage solution for image uploading – the app is developed to seamlessly work with a self-hosted [Directus](https://directus.io) instance and local development/hosting storage; however, you can disable this function or set up/develop your own storage solution!

_It has been successfully deployed on Vercel!_ (thanks to a postbuild script located in package.json). Have not been able to properly test it on Netlify. Beware, the Sharp module, used for image processing, seems to cause deployment crashes / images won't be uploaded. In any case, it should run in a local/pm2 environment (tested on MacOS and Arch Linux).

If you're familiar with the 'secret santa problem', you'll know **the game will fail at some point**. This app uses a mix of mongoose permissions and 'underscore sample' to randomise the results and avoid the game to fail. Yet, it is not guaranteed to work all the time. Therefore, testing tools have been added to allow the admin to **easily reset the game without deleting any of the user data!**

# Tasks to do / Ideas to develop:

- Organise the code.
- Done! ~~Add logout (as of now just delete the cookie~~
- Done! ~~Add translations _as of now it has a mix of english and spanish~~
- Done! ~~Organise global envs (using [config] until Vite 2.7 is released)~~
- Add the possibility to have multiple admins/multiple games..?
- ThreeJS snow animation

Check the **env.example** file for instructions on the env vars to set before deploying the project.
Set a default language before deploying, this can be adjusted in the admin panel afterwards.

**Open to contributions!**

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open

# or start the server with open ports
npm run dev -- --host

```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
