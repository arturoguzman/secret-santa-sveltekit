import preprocess from 'svelte-preprocess';
/* import adapter from '@sveltejs/adapter-auto'; */
import vercel from '@sveltejs/adapter-vercel';
import { resolve } from 'path';
/* import adapter from '@sveltejs/adapter-netlify'; */

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: vercel(),

		// hydrate the <div id="svelte"> element in src/app.html
		target: '#svelte',
		vite: {
			resolve: {
				alias: {
					$lib: resolve('./src/lib'),
					$media: resolve('./src/media')
				}
			},
			define: {
				'process.env': process.env
			},
			optimizeDeps: {
				//   include: ['sharp']  - also tried include
				exclude: ['sharp']
			},
			ssr: {
				external: ['sharp']
			}
		}
	},

	preprocess: [
		preprocess({
			postcss: true
		})
	]
};

export default config;
