const config = {
	mode: 'jit',
	purge: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		extend: {
			colors: {
				emerald: {
					100: '#E8F2EA',
					200: '#BBD7C1',
					300: '#8DBC98',
					400: '#5FA16F',
					500: '#1B7931',
					600: '#166127',
					700: '#10491D',
					800: '#0B3014',
					900: '#05180A'
				}
			}
		}
	},

	plugins: []
};

module.exports = config;
